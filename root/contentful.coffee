module.exports =
  access_token: 'a05d9a2fb7bd363ae5e2ebcece95c9bff641c07b0cc0e22b9db9c9b47c457030'
  management_token: 'e855092df3a07810540bd0f7a5d8d110464a52c0c6b0a50edabf7184043e4638'
  space_id: 'ckogtlvkvnez'
  content_types:                   # remove these object braces once the config below is filled out
    posts:                          # data will be made available through this key on the `contentful` object in your templates
    	id: '2wKn6yEnZewu2SCCkus4as'                    # ID of your content type
    #   filters: {}                   # passes filters to the call to contentful's API, see contentful's docs for more info
    #   template: 'path/to/template'  # if present a single page view will be created for each entry in the content type
    #   path: (entry) ->              # override function for generating single page file path, passed in the entry object

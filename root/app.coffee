js_pipeline  = require 'js-pipeline'
sass = require 'roots-sass'
handlebars = require 'handlebars'
layouts = require 'handlebars-layouts'
dynamic = require 'dynamic-content'
config       = require './contentful'
contentful = require 'roots-contentful'
loadPartials = require './views/_utils/load-partials'
loadHelpers  = require './views/_utils/load-helpers'

# Register handlebars-layouts
layouts.register(handlebars);

module.exports =
  ignores: ['readme.md', '**/layout.*', '**/_*', '.gitignore', 'ship.*conf', 'contentful.coffee']

  extensions: [
    js_pipeline(files: 'assets/js/*.js'),
    sass(files: "assets/css/main.scss", out: 'css/site.css', style: 'compressed')
    contentful(config)
  ]

  scss:
    sourcemap: true

  server:
    clean_urls: true

  handlebars:
    helpers: loadHelpers(__dirname + '/views/_helpers')
    partials: loadPartials(__dirname + '/views/_partials')